## sisteminha simples para salvar leads e enviar ebook aos que se cadastram no site
### Fazendo funcionar...

- crie uma pasta chamada 'Ietzz' na aréa de trabalho
- Faça o Git clone desse projeto para dentro da pasta
- Execute o Comando para Criar o Banco de Dados:
```sh
$ docker run -d --name dbserver.mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql:5.7
```
- Execute o Comando para Criar o Servidor: 
`Não esqueça de trocar o [username] pelo nome de usuario do servidor`
```sh
docker run -d --name projetophp7 -p 8080:80 --link dbserver.mysql:mysql -v /home/[username]/Desktop/ietzz:/var/www/html nimmis/apache-php7
```

- Acesse o terminal MySQL com o comando: 
```sh
docker exec -it dberver.mysql bash
```
`Se houver erro pegue o ID do container docker referente a "dberver.mysql" com o comando:`

```sh
docker ps
```
`e cole no lugar de dbserver.mysql ficando:`
```sh
docker exec -it [id_do_container_docker] bash
```
- após acessar o container execute os comandos:
```sh
mysql -u root -p
```
- Irá pedir a senha: digite "root"
- em Seguida:
```sh
create database lead
use lead
```
cole o codigo SQL do banco de dados:
```sh
create table leads (
	id int not null primary key auto_increment,
	email varchar(250),
	name varchar(250),
	data_nascimen date,
	estado varchar(250),
	cidade varchar(250),
	cozinhar varchar(250)
);
```

