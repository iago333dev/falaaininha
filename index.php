<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Fala Ai Ninha || Ebook </title>
		
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>	
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">

		
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>

				<!-- wrapper -->



					<img src="./img/falaininha.jpg" style="
width: 100%;
height: 100%;
background: url('../img/falaininha.jpg');
">









		<!-- wrapper -->
		<div class="wrapper">


			<!-- banner -->


			<div class="subscribe">
            <?php

if($_GET["save"] == 'true'){
    echo "<div class='alert alert-success' role='alert'>
   Cadastro realizado com sucesso
  </div>";

  header("Content-type:application/pdf");

  // It will be called downloaded.pdf
  header("Content-Disposition:attachment;filename=ebook-receitasdemaainha.pdf");
  
  // The PDF source is in original.pdf
  readfile("ebook.pdf");
}

?>

				<div class="container">

					<div class="sub-content">

						<div class="row">

							<div class="col-md-6 col-sm-6">
								<!-- subscribe update message -->
								<div class="sub-message">

									<!-- heading -->
									<h3>Apresentação</h3>
									<!-- paragraph -->
									<p>O fato de morar sozinha me trouxe a destreza para cozinhar, afinal mainha sempre me disse, "Quem gosta de comer bem, aprende a cozinhar bem!" Então, comecei a me arriscar na cozinha depois que vim morar sozinha em São Paulo, da brincadeira de compartilhar no Instagram surgiu a ideia de criar esse e-book com algumas receitas de minha mainha, chamei a melhor amiga e embarcamos nessa juntas. O intuito do e-book é ajudar quem ta afim de se arriscar na cozinha, assim como eu.
Nesse e-book mostro que não precisamos de muito para comer bem, gostoso e saudável, ovos, leite, farinha de trigo... e criatividade fazem a diferença ao nível de você ter prazer em cozinhar e gastando pouco para isso. Afinal, comer é um dos maiores prazeres da vida e me aventuro em dizer que, comer deve ser o maior prazer da vida, as receitas que trago aqui de minha mainha está para provar isso.</p>
									<!-- update form  -->
									<form role="form" method="get" action="ebook.html">
										<div class="input-group">
											
											<span class="input-group-btn">
												<button class="btn btn-info btn-lg" type="submit">Download do Ebook</button>
											</span>
										</div><!-- /input-group -->
									</form>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<img src="./img/meuamoor.jpg" alt="Girl in a jacket" style="max-width: 100%; max-height:100%;">
							</div>
						</div>
					</div>
				</div>
			</div>
						
						<div class="footer-above">
				<div class="container">
					<!-- footer above main content -->
					<div class="above-content">
                        <h4>"Quem gosta de comer bem, aprende a cozinhar bem!" - Cida Brito (mainha)</h4>

						<div class="social">
							<p>Siga o Fala Ai Ninha!? nas redes sociais</p>
							<a href="https://www.facebook.com/Falaaininha/"><i class="fa fa-facebook"></i></a>
							<a href="https://www.instagram.com/falaaininha/"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/channel/UCNFv8O3ENe2fcLTeKl9R4oA"><i class="fa fa-youtube-play"></i></a>
						</div>
					</div>
				</div>
			</div>
			
			<!-- footer -->
			<footer>
				<div class="container">
					<!-- navigation link -->
					<p class="nav-link"><a href="#">Contact</a> &nbsp;|&nbsp; <a href="#">Downloads</a> &nbsp;|&nbsp; <a href="#">Email</a> &nbsp;|&nbsp; <a href="#">Support</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></p>
					<!-- copy right -->
					<!-- This theme comes under Creative Commons Attribution 4.0 Unported. So don't remove below link back -->
					<p class="copy-right">Copyright &copy; 2020 <a href="#">Fala Ai Ninha?!</a> All rights reserved. </p>
				</div>
			</footer>
			
		</div>
			
			
		

		
		
		<!-- Javascript files -->
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Respond JS for IE8 -->
		<script src="js/respond.min.js"></script>
		<!-- HTML5 Support for IE -->
		<script src="js/html5shiv.js"></script>
		<!-- Custom JS -->
		<script src="js/custom.js"></script>
	</body>	
</html>